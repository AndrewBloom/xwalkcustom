package com.goodboy.plugin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Typeface;
import android.widget.Toast;
import android.app.Activity;
import android.view.WindowManager;

import org.xwalk.core.XWalkView;

public class BrowserDialogGBD extends Dialog {

	boolean immersive = true;

    public BrowserDialogGBD(Context context, int theme, boolean immersive) {
        super(context, theme);
	this.immersive = immersive;
    }

	@Override
	public void show() {
		if (immersive) { 
			// Set the dialog to not focusable.
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
		        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
			
		    	if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
		        	getWindow().getDecorView().setSystemUiVisibility(
		                	View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
		                        	View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
		                        	View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
		                        	View.SYSTEM_UI_FLAG_FULLSCREEN |
		                        	View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
		                        	View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		    	}
		    	// Show the dialog with NavBar hidden.
		    	super.show();
			
		    	// Set the dialog to focusable again.
		    	getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
		} else super.show();
	}
}
