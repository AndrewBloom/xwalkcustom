package com.goodboy.plugin;

import com.goodboy.plugin.BrowserDialogGBD;

import android.content.res.Resources;
import org.apache.cordova.*;
import org.apache.cordova.PluginManager;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import org.xwalk.core.XWalkView;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.internal.XWalkViewInternal;
import org.xwalk.core.XWalkCookieManager;

import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageButton;
import android.graphics.Color;
import java.io.IOException;
import java.io.InputStream;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.graphics.Typeface;
import android.widget.Toast;

import android.webkit.WebResourceResponse;

public class InAppBrowserGBDXwalk extends CordovaPlugin {

    private BrowserDialogGBD dialog;
    private XWalkView xWalkWebView;
    private CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if(action.equals("open")) {
            this.callbackContext = callbackContext;
            this.openBrowser(data);
        }

        if(action.equals("close")) {
            this.closeBrowser();
        }

        if(action.equals("show")) {
            this.showBrowser();
        }

        if(action.equals("hide")) {
            this.hideBrowser();
        }

        return true;
    }

    class MyResourceClient extends XWalkResourceClient {
           MyResourceClient(XWalkView view) {
               super(view);
           }

           @Override
           public void onLoadStarted (XWalkView view, String url) {
               try {
                   JSONObject obj = new JSONObject();
                   obj.put("type", "loadstart");
                   obj.put("url", url);
                   PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
                   result.setKeepCallback(true);
                   callbackContext.sendPluginResult(result);
               } catch (JSONException ex) {}
           }

           @Override
           public void onLoadFinished (XWalkView view, String url) {
               try {
                   JSONObject obj = new JSONObject();
                   obj.put("type", "loadstop");
                   obj.put("url", url);
                   PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
                   result.setKeepCallback(true);
                   callbackContext.sendPluginResult(result);
               } catch (JSONException ex) {}
           }
   }

    private void openBrowser(final JSONArray data) throws JSONException {
        final String url = data.getString(0);
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int margin[] = {5, 5, 5, 5};
                String imgPath = "www/images/popup_close.png";
                int minSize = 28;
                boolean hiddenButton = false;
                boolean openHidden = false;
		int corner = 1;
		boolean immersive = true;

                if(data != null && data.length() > 1) {
                    try {
                            JSONObject options = new JSONObject(data.getString(1));

                            if(!options.isNull("buttonCorner")) {
				String c = options.getString("buttonCorner");
				if (c.equals("topLeft")) corner = 0;
				else if (c.equals("topRight")) corner = 1;
				else if (c.equals("bottomLeft")) corner = 3;
				else if (c.equals("bottomRight")) corner = 2;
                            }
                            if(!options.isNull("imgPath")) {
                                imgPath = options.getString("imgPath");
                            }
                            if(!options.isNull("marginLeft")) {
                                margin[0] = options.getInt("marginLeft");
                            }
                            if(!options.isNull("marginTop")) {
                                margin[1] = options.getInt("marginTop");
                            }
                            if(!options.isNull("marginRight")) {
                                margin[2] = options.getInt("marginRight");
                            }
                            if(!options.isNull("marginBottom")) {
                                margin[3] = options.getInt("marginBottom");
                            }
                            if(!options.isNull("minSize")) {
                                minSize = options.getInt("minSize");
                            }
                            if(!options.isNull("immersive")) {
                                immersive = options.getBoolean("immersive");
                            }
                            if(!options.isNull("openHidden")) {
                                openHidden = options.getBoolean("openHidden");
                            }
                            if(!options.isNull("hiddenButton")) {
                                hiddenButton = options.getBoolean("hiddenButton");
                            }
                        }
                    catch (JSONException ex) {

                    }
                }
                dialog = new BrowserDialogGBD(cordova.getActivity(), android.R.style.Theme_NoTitleBar_Fullscreen, immersive);
                xWalkWebView = new XWalkView(cordova.getActivity(), cordova.getActivity());
                XWalkCookieManager mCookieManager = new XWalkCookieManager();
                mCookieManager.setAcceptCookie(true);
                mCookieManager.setAcceptFileSchemeCookies(true);
                xWalkWebView.setResourceClient(new MyResourceClient(xWalkWebView));
                xWalkWebView.load(url, "");


	RelativeLayout relLayout = new RelativeLayout(cordova.getActivity());
        LayoutParams relLayoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); 
	RelativeLayout.LayoutParams topRightGravityParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	switch (corner) {
		case 0:
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			break;
		case 1:
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			break;
		case 2:
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			break;
		case 3:
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			break;
		default:
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        		topRightGravityParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	}

        final float scale = cordova.getActivity().getResources().getDisplayMetrics().density;
	topRightGravityParams.setMargins((int)(margin[0]*scale+0.5f),(int)(margin[1]*scale+0.5f),(int)(margin[2]*scale+0.5f),(int)(margin[3]*scale+0.5f));
        
        ImageButton btn = new ImageButton(cordova.getActivity());
        btn.setBackgroundColor(Color.TRANSPARENT);
        btn.setAdjustViewBounds(true);

        try {
            InputStream istr = cordova.getActivity().getAssets().open(imgPath);
            //set drawable from stream
            btn.setBackground(Drawable.createFromStream(istr, null));
        } catch (IOException e) {
            e.printStackTrace();
        }
        btn.setMinimumWidth((int)(minSize*scale+0.5f));
        btn.setMinimumHeight((int)(minSize*scale+0.5f));
	btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        closeBrowser();
                    }
                });
        relLayout.addView(xWalkWebView, relLayoutParam);
        if (!hiddenButton)relLayout.addView(btn, topRightGravityParams);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
                dialog.setCancelable(true);
                LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                dialog.addContentView(relLayout, relLayoutParam);
                if(!openHidden) {
                    dialog.show();
                }
            }
        });
    }

    public void hideBrowser() {
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(dialog != null) {
                    dialog.hide();
                }
            }
        });
    }

    public void showBrowser() {
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(dialog != null) {
                    dialog.show();
                }
            }
        });
    }

    public void closeBrowser() {
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                xWalkWebView.onDestroy();
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("type", "exit");
                    PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
                    result.setKeepCallback(true);
                    callbackContext.sendPluginResult(result);
                } catch (JSONException ex) {}
            }
        });
    }
}
